#include <stdio.h>
#define MAXLINE 1000 /* maximum length of string */

/* function prototype */
void reverse(char source[], char target[]);

int main(void) {
  char original[] = "This is a test: can you print me in reverse character order?";
  char reversed[MAXLINE];

  printf("%s\n", original);
  reverse(original, reversed);
  printf("%s\n", reversed);
  return 0;
}

/* reverse the order of characters in 'source', write to 'target'.
   Assume 'target' is big enough. */
void reverse(char source[], char target[]) {
	
	long i=0;
	long n;
		
	while(source[i]!='\0'){
		i++;
	}		
	
	n=i-1;
	
/*	printf("%ld\n",i);*/
	
	for(n=i-1;n>=0;n--){
		printf("%c",source[n]);	
	}
	
	
	
	/*for(n==i;n>=0;n--){
	}*/
	
}