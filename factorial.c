#include <stdio.h>
#include <math.h>
#include <limits.h>

long maxlong(void){
	
	return LONG_MAX; 
}

double upper_bound(long n){
	
	double n2;
	
	if (n<6){
		n2=719;
	}
	else{
		n2=pow((n/2.0),n);	
	}
		
	return(n2);
}

long factorial (long n){
	
	long a=1.;
	long a2;
	
		if(n<0){
			a=-2.;
		}
		else{
			if(upper_bound(n)>maxlong()){
				a=-1.;
			}
				else{
					for (a2=1.;a2<=n;a2++){
						a=a*a2;					
				}
			}
		}
	
	return a;

	
}

int main(void) {
    long i;
	
    /* The next line should compile once "maxlong" is defined. */
    printf("maxlong()=%ld\n", maxlong());

    /* The next code block should compile once "upper_bound" is defined. */

	
	
    for (i=0; i<10; i++) {
        printf("upper_bound(%ld)=%g\n", i, upper_bound(i));
		printf("factorial(%ld)=%ld\n",i,factorial(i));
    }
		
  
	return 0;
}















